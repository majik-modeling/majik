class CreateFilters < ActiveRecord::Migration
  def change
    create_table :filters do |t|
      t.integer :user_id
      t.string :filter_path
      t.string :filter_name

      t.timestamps
    end
  end
end
