class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :image_name
      t.integer :parent_id
      t.integer :user_id
      t.string :version
      t.string :image_path
      t.integer :filter_id

      t.timestamps
    end
  end
end
