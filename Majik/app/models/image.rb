class Image < ActiveRecord::Base
	belongs_to :user
	mount_uploader :image_path, ImagePathUploader # Tells rails to use this uploader for this model.
    validates :image_name, presence: true # Make sure the owner's name is present.
end
