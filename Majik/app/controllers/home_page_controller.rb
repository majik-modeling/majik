class HomePageController < ApplicationController
  def home
  	@images = Image.paginate(page: params[:page], per_page: 3)
  end

  def help
  end

  def about
  end
end
